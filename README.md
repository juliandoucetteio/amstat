# amastat

Documents and scripts outlining my solutions to several problems while archiving Facebook messages, interactions, and Google Documents in August 2015

## Prefix

I have approximately 50,000 private messages of sentimental value containing about 700 personal photos, 100 attachments, and many links to media that I would like to archive for sentimental value, to create statistics, and to make them more searchable.

I also have a Google Drive subscription containing a shared folder with a large amount of media and documents mixed.

## Problems

* Facebook's downloadable archive doesn't contain: 
 * All of my messages (only some)
 * All of the photos in my messages
 * Proper text spacing
 * Stickers in messages
 * Emoticons in messages
 * Mobile interactions like making a FB messenger call
* Google drive's download function has size limits
* Google drive's document export doesn't contain editing history

## 0.0.1 Scope

- [ ]  Create injection script for exporting facebook messages HTML and CSS